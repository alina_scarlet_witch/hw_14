﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Security;
using NUnit.Framework;
using System.Data;

namespace hw
{
    public class Test

    {
        SqlConnector sqlConnector = new SqlConnector("alina", "alina");

        [TestCase("Rothfuss",0,2)]
        [TestCase("Cote", 14, 2)]

        public void CheckSelectFromPersonsInDataBase(string exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex].ToString());
        }

        [TestCase(14, 18, 1)]
        [TestCase(170, 10, 2)]

        public void CheckSelectFromOrdersInDataBase(int exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM orders");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex]);
        }


        [TestCase("Darline", 0, 1)]
        [TestCase("London", 3, 4)]

        public void CheckSelectFromPersonsInnerJoinInDataBase(string exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons INNER JOIN orders ON ID = ID_persons");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex].ToString());
        }


        [TestCase(161, 10, 7)]
        [TestCase(53, 6, 3)]

        public void CheckSelectFromPersonsLeftJoinInDataBase(int exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons LEFT JOIN orders ON ID = ID_persons");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex]);
        }


        [TestCase("Martin", 20, 2)]
        [TestCase("Tamma", 8, 1)]

        public void CheckSelectFromPersonsRightJoinInDataBase(string exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons RIGHT JOIN orders ON ID = ID_persons");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex].ToString());
        }


        [TestCase("Noel ", 11, 1)]
        [TestCase("Milan", 11, 4)]

        public void CheckSelectFromPersonsFullJoinInDataBase(string exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons FULL JOIN orders ON  ID = ID_persons");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex].ToString());
        }


        [TestCase(307, 7, 1)]
        [TestCase(212, 10, 1)]

        public void CheckSelectFromPersonsGroupByInDataBase(int exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT ID_persons, SUM(Sum_orders) as sum FROM orders GROUP BY ID_persons");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex]);

        }

        [TestCase("Porto", 1, 4)]
        [TestCase("Swift", 2, 2)]

        public void CheckSelectFromPersonsOrderByAgeInDataBase(string exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons ORDER BY Age DESC");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex]);

        }

        [TestCase("Isma", 17, 1)]
        [TestCase("Lott", 14, 2)]

        public void CheckSelectFromPersonsOrderByCityInDataBase(string exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM persons ORDER BY City");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex]);

        }

        [TestCase(192, 20, 2)]
        [TestCase(36, 3, 2)]

        public void CheckSelectFromPersonsOrderBySumOrdersInDataBase(int exResult, int rowsIndex, int columnIndex)
        {
            sqlConnector.ConnectToCatalog("database_HW");
            DataTable result = sqlConnector.Execute("SELECT * FROM orders ORDER BY Sum_orders");
            Assert.AreEqual(exResult, result.Rows[rowsIndex].ItemArray[columnIndex]);

        }

       
    }
}
