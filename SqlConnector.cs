﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace hw
{
    public class SqlConnector
    {
        private readonly SqlCredential _credential;

        public SqlConnector(string login, string password)
        {
            var credential = new SecureString();
            for (var i = 0; i < password.Length; i++)
                credential.InsertAt(i, password[i]);
            credential.MakeReadOnly();

            _credential = new SqlCredential(login, credential);
        }
       public string _connectionString;
        public void ConnectToCatalog(string catalogName)
        {
            _connectionString = @"Data Source=DESKTOP-3OBK0S9\SQLEXPRESS;" +
                $"Initial Catalog = { catalogName};";
        }

        public DataTable Execute(string SqlRequest)
        {
            var dataSet = new DataSet();
            using (var sqlConnection = new SqlConnection(_connectionString, _credential))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(SqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = SqlRequest;
                    var adapter = new SqlDataAdapter(sqlCommand);
                    adapter.Fill(dataSet);
                }
            }
            return dataSet.Tables[0];
        }
    }
}
